#!/bin/sh

CURL=/usr/bin/curl
GREP=/bin/grep
ECHO=/bin/echo
DATE=/bin/date

LIST_URL=https://www.hello-matrix.net/public_servers.php
SECRET_KEY=enter secret key here


# Print date
$DATE

# Loop through servers
$CURL -s $LIST_URL?format=plain | \
  $GREP -o -E "^[a-zA-Z0-9\.:-]+" | \
   while read SERVER
   do
    $ECHO "Getting room count from $SERVER..."
   
    $CURL -s --max-time 60 -w "\nDATA: %{http_code} %{time_starttransfer} %{time_namelookup}\nDATA: $SERVER\nDATA: $SECRET_KEY\nDATA: ROOM_COUNT\n" -X GET --header 'Accept: application/json' https://$SERVER/_matrix/client/r0/publicRooms | \
     $GREP -E "(^DATA:|total_room_count_estimate)" |
      $CURL -s --max-time 10 -X POST --data-binary @- $LIST_URL
   done
   
$ECHO Done.

# Print newlines
$ECHO 
$ECHO 

