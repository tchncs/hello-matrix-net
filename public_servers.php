<?php

require('public_servers.conf.php');

$sslGradeMap = array(
  "A+" => 90,
  "A"  => 100,
  "A-" => 110,
  "B"  => 200,
  "C"  => 300,
  "D"  => 400,
  "E"  => 500,
  "F"  => 600,
  "T"  => 800,
  "M"  => 900
 );

$show_from = $default_show_from;
if(isset($_GET['show_from']) && in_array($_GET['show_from'], $secret_keys)) {
  $show_from = $_GET['show_from'];
}
 
// Gather timing statistics 
$timing_start = microtime(true)*1000;


// Open database
try {
  $db = new PDO('mysql:host='.$db_host.';dbname='.$db_name.';charset=utf8mb4', $db_user, $db_pass, array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
  
  if($_SERVER['REQUEST_METHOD'] == 'POST' && preg_match("/^update_users=([a-zA-Z\-0-9\.:]+)$/", $_SERVER['QUERY_STRING'], $matches)) {
    
    // Does the hostname exist in our database?
    $hostname = $matches[1];
    
    $stmt = $db->prepare("SELECT COUNT(*) AS server_exists FROM servers WHERE hostname=? AND active=1");
    $stmt->execute(array($hostname));
    $result = $stmt->fetch();
    if(!$result['server_exists']) {
        http_response_code(401);
        exit("Wrong request.");
    }
    
    // Does the hostname match to the remote address?
    if(strstr($_SERVER['REMOTE_ADDR'], ":") !== false) {
      // IPv6 address
      if(!strstr(shell_exec("host -t AAAA $hostname"), $_SERVER['REMOTE_ADDR'])) {
        http_response_code(401);
        exit("Wrong request.");
      }
    } else {
      // IPv4 address
      if(!in_array($_SERVER['REMOTE_ADDR'], gethostbynamel($hostname))) {
        http_response_code(401);
        exit("Wrong request.");
      }
    }
    
    // Ok, everything matches, we parse the data provided.
    $users_active = 0;
    $users_deactivated = 0;
    $users_bridged = 0;
    $guests = 0;
    
    $lines = file("php://input");
    foreach($lines as $line) {
      $cols = explode("|", $line);
      
      // Old version of reporting
      if(count($cols) == 3) {
        // This matches.
        if($cols[0] == '0' && $cols[1] == 'f') {
          // Active users
          $users_active = $cols[2];
        } else if($cols[0] == '0' && $cols[1] == 't') {
          // Deactivated users
          $users_deactivated = $cols[2];
        } else if($cols[0] == '1') {
          // Guest users
          $guests += $cols[2];
        }
      }
      
      // New version of reporting
      if(count($cols) == 4) {
        // This matches.
        if($cols[0] == '0' && $cols[1] == 'f' && $cols[2] == 'f') {
          // Active users
          $users_active = $cols[3];
        } else if($cols[0] == '0' && $cols[1] == 't') {
          // Deactivated users
          $users_deactivated = $cols[3];
        } else if($cols[0] == '0' && $cols[1] == 'f' && $cols[2] == 't') {
          // Bridged users
          $users_bridged = $cols[3];
        } else if($cols[0] == '1') {
          // Guest users
          $guests += $cols[3];
        }
      }
            
    }
    
    // Did we receive a zero report? Log as error and exit.
    if($users_active == 0) {
      $stmt = $db->prepare("INSERT INTO error_log (error_type, error_content) VALUES (?, ?)");
      $stmt->execute(array('Zero User Count - '.$hostname, join('', $lines)));
      
      exit("Ignored zero report.");
    }
    
    
    // Include in database
    $db->beginTransaction();
    
    // Set all other records to outdated
    $stmt = $db->prepare("UPDATE server_users SET current=0 WHERE hostname=?");
    $stmt->execute(array($hostname));
    
    // Add this record
    $stmt = $db->prepare("INSERT INTO server_users (hostname, current, users_active, users_deactivated, users_bridged, guests, from_remote_addr) VALUES (?, 1, ?, ?, ?, ?, ?)");
    $stmt->execute(array($hostname, $users_active, $users_deactivated, $users_bridged, $guests, $_SERVER['REMOTE_ADDR']));
    
    // Commit
    $db->commit();
    
    // Done.
    echo "Done.\n";
  
  } else if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
    // We have received timing or rooms data.
    $lines = file("php://input");
    
    // What have we received?
    $type_of_request = trim(str_replace("DATA: ", "", array_pop($lines)));
    
    if($type_of_request != "TIME" && $type_of_request != "ROOM_COUNT" && $type_of_request != "REGISTRATION_TEST" && $type_of_request != "SERVER_VERSION" && $type_of_request != "SSLLABS_RESULTS") {
      http_response_code(401);
      exit("Wrong request.");
    }
    
    // Is the secret key (in the second last line) correct?
    $secret_key = trim(str_replace("DATA: ", "", array_pop($lines)));
    if(!isset($secret_keys[$secret_key])) {
      http_response_code(401);
      exit("Wrong request.");
    }
    $measured_from = $secret_keys[$secret_key];
    
    // Did we receive a valid hostname?
    $hostname = trim(str_replace("DATA: ", "", array_pop($lines)));
    if(!preg_match("/^([a-zA-Z\-0-9:\.]+)$/", $hostname)) {
      http_response_code(401);
      exit("Wrong request.");
    }
    
    // Did we receive timing or error information?
    if(!preg_match("/^([0-9]+) ([0-9\.,]+) ([0-9\.,]+)$/", trim(str_replace("DATA: ", "", array_pop($lines))), $results)) {
      http_response_code(401);
      exit("Wrong request.");
    }
    
    // Let's convert results data by replacing "," with "." if necessary (depends on locale of curl)
    $results[2] = str_replace(",", ".", $results[2]);
    $results[3] = str_replace(",", ".", $results[3]);
    
   
    // Okay, what are we checking here?
    if($type_of_request == "TIME") {
      // TIMING DATA
   
      // Did we receive versions?
      $received_data = json_decode(implode("\n", $lines), true);
    
      if(!$received_data || !isset($received_data['versions']) || !count($received_data['versions'])) {
        $matrix_versions = "";
      } else {
        $matrix_versions = implode(", ", $received_data['versions']);
      }
  
       
      // Ok, let's update the detailed data and the summary statistics for that server.
      $db->beginTransaction();
    
      // What was the last status of this server?
      $stmt = $db->prepare("SELECT t1.hostname, t1.notif_email, t2.last_response FROM servers t1 LEFT JOIN server_statistics t2 ON t2.hostname=t1.hostname AND t2.measured_from=? WHERE t1.hostname=?");
      $stmt->execute(array($measured_from, $hostname));
      $server_details = $stmt->fetch();
    
      // Does the server exist?
      if(!$server_details) {
        http_response_code(401);
        exit("Wrong request.");
      }
    
      // Insert timings
      $stmt = $db->prepare("INSERT INTO server_timings (hostname, response_code, response_time, versions, measured_from) VALUES (?, ?, ?, ?, ?)");
      $stmt->execute(array($hostname, $results[1], max(0,$results[2]-$results[3])*1000, $matrix_versions, $measured_from));
    

      if($results[1] == 200) {
        // Successful request
        $stmt = $db->prepare("UPDATE server_statistics SET status_since=IF(last_response=200,status_since,NOW()), last_response=?, last_response_time=?, last_versions=?, measurements=measurements+1, successful=successful+1, sum_response_time=(sum_response_time + ?), measurements_short=measurements_short+1, successful_short=successful_short+1, sum_response_time_short=(sum_response_time_short + ?) WHERE hostname=? AND measured_from=?");
        $stmt->execute(array($results[1], max(0,$results[2]-$results[3])*1000, $matrix_versions, max(0,$results[2]-$results[3])*1000, max(0,$results[2]-$results[3])*1000, $hostname, $measured_from)); 
      
        // Does a statistic entry exist? Otherwise retry with INSERT.
        if($stmt->rowCount() < 1) {
          $stmt = $db->prepare("INSERT INTO server_statistics (hostname, last_response, last_response_time, status_since, last_versions, measured_from, measurements, successful, sum_response_time, measurements_short, successful_short, sum_response_time_short) VALUES (?, ?, ?, NOW(), ?, ?, 1, 1, ?, 1, 1, ?)");
          $stmt->execute(array($hostname, $results[1], max(0,$results[2]-$results[3])*1000, $matrix_versions, $measured_from, max(0,$results[2]-$results[3])*1000, max(0,$results[2]-$results[3])*1000)); 
        }
      
        // Are we online after an outage?
        // Only send mails from default_show_from to avoid multiple emails!
        if($server_details['last_response'] != 200 && $server_details['notif_email'] && $measured_from == $default_show_from) {
          // Notify notification contact that we are back online
          mail($server_details['notif_email'], "Matrix server ".$hostname." is back online", "The Matrix server list has received a positive response from the server again, so the server is now back to being classified as 'online'.", "From: Matrix Server List <".$warnings_email.">");
        }
  
      } else {
        // Unsuccessful request
        $stmt = $db->prepare("UPDATE server_statistics SET status_since=IF(last_response=200,NOW(),status_since), last_response=?, last_response_time=NULL, measurements=measurements+1, measurements_short=measurements_short+1 WHERE hostname=? AND measured_from=?");
        $stmt->execute(array($results[1], $hostname, $measured_from)); 
        
        // Does a statistic entry exist? Otherwise retry with INSERT.
        if($stmt->rowCount() < 1) {
          $stmt = $db->prepare("INSERT INTO server_statistics (hostname, last_response, last_response_time, status_since, last_versions, measured_from, measurements, successful, sum_response_time, measurements_short, successful_short, sum_response_time_short) VALUES (?, ?, NULL, NOW(), '', ?, 1, 0, 0, 1, 0, 0)");
          $stmt->execute(array($hostname, $results[1], $measured_from)); 
        }
      
        // Are we newly offline?
        if($server_details['last_response'] == 200 && $server_details['notif_email'] && $measured_from == $default_show_from) {
          // Notify notification contact that we are offline
          mail($server_details['notif_email'], "Matrix server ".$hostname." is OFFLINE", "The Matrix server list has received a failure response (".$results[1].") while trying to access this server. The server is now classified as 'OFFLINE'.", "From: Matrix Server List <".$warnings_email .">");
        }      
      
      }
        
      // Commit changes.
      $db->commit();
      
    } else if($type_of_request == "ROOM_COUNT") {
      // ROOM COUNT DATA.
      
      // Have we received valid room count data?
      if(!preg_match("/\"total_room_count_estimate\"\s*:\s*\"?([0-9]+)\"?/", implode("", $lines), $matched)) {
        http_response_code(401);
        exit("Wrong request.");
      }
      
      // Okay, we have room count data.
      $room_count = $matched[1];
      
      // Update the database.
      $db->beginTransaction();
      
      // Mark old room counts as not current
      $stmt = $db->prepare("UPDATE server_room_count SET current=0 WHERE hostname=? AND current=1");
      $stmt->execute(array($hostname)); 
      
      // Insert new room count
      $stmt = $db->prepare("INSERT INTO server_room_count (hostname, current, response_code, response_time, public_room_count, measured_from) VALUES (?, 1, ?, ?, ?, ?)");
      $stmt->execute(array($hostname, $results[1], max(0,$results[2]-$results[3])*1000, $room_count, $measured_from));
            
      // Commit changes.
      $db->commit();      
    
    } else if($type_of_request == "SERVER_VERSION") {
      // SERVER VERSION FROM FEDERATION ENDPOINT.
      
      $jsonData = implode("", $lines);
      
      // Server version
      if(!preg_match("/\"version\"\s*:\s*\"([^\"]*)\"/", $jsonData, $matched)) {
        http_response_code(401);
        exit("Wrong request.");      
      }
      $server_version = $matched[1];
      
      // Server name
      if(!preg_match("/\"name\"\s*:\s*\"([^\"]*)\"/", $jsonData, $matched)) {
        http_response_code(401);
        exit("Wrong request.");      
      }
      $server_name = $matched[1];
      
      // Update the database.
      $db->beginTransaction();
      
            
      // Update all existing entries to old
      $stmt = $db->prepare("UPDATE server_version SET current=0 WHERE hostname=?");
      $stmt->execute(array($hostname)); 
      
      // Insert new server name and version
      $stmt = $db->prepare("INSERT INTO server_version (hostname, current, server_name, server_version, response_code, response_time, measured_from) VALUES (?, 1, ?, ?, ?, ?, ?)");
      $stmt->execute(array($hostname, $server_name, $server_version, $results[1], max(0,$results[2]-$results[3])*1000, $measured_from));
            
      // Commit changes.
      $db->commit();           
      
      
    
    } else if($type_of_request == "SSLLABS_RESULTS") {
      // Process SSLLabs results
      
      $jsonResults = json_decode(implode("", $lines), true);
                       
      $minGrade = "";
      $minGradeTrustIgnored = "";
      $hasWarnings = false;
      
      if(!is_array($jsonResults) || !isset($jsonResults[0]) || !isset($jsonResults[0]['endpoints']) || !is_array($jsonResults[0]['endpoints'])) {
        http_response_code(401);
        exit("Wrong request."); 
      }
      
      foreach($jsonResults[0]['endpoints'] as $endpoint) {
        if($minGrade == "" || $sslGradeMap[$endpoint['grade']] > $sslGradeMap[$minGrade]) {
          $minGrade = $endpoint['grade'];
        }
        
        if($minGradeTrustIgnored == "" || $sslGradeMap[$endpoint['gradeTrustIgnored']] > $sslGradeMap[$minGradeTrustIgnored]) {
          $minGradeTrustIgnored = $endpoint['gradeTrustIgnored'];
        }
        
        if($endpoint['hasWarnings']) {
          $hasWarnings = true;
        }  
      }
      
      // Update the database.
      $db->beginTransaction();
      
            
      // Update all existing entries to old
      $stmt = $db->prepare("UPDATE server_security SET current=0 WHERE hostname=?");
      $stmt->execute(array($hostname)); 
      
      // Insert new server name and version
      $stmt = $db->prepare("INSERT INTO server_security (hostname, current, grade, gradeTrustIgnored, hasWarnings) VALUES (?, 1, ?, ?, ?)");
      $stmt->execute(array($hostname, $minGrade, $minGradeTrustIgnored, $hasWarnings));
            
      // Commit changes.
      $db->commit();           
      
    
    } else if($type_of_request == "REGISTRATION_TEST") {
      // Registration test - does the registration return 403?
      
      // If it returns 403, this server is not public after all => we send a warning email.
      if($results[1] == 403) {
        mail($warnings_email, "Matrix server ".$hostname." not public", "The Matrix server list has received a 403 response while trying to register a user on this server. Please verify whether this server is really still public.", "From: Matrix Server List <".$warnings_email.">");
      }
      
    }
    
    // Return.
    header("Content-type: text/plain");
    echo "Done.\n";

    
  } else if(isset($_GET['format']) && $_GET['format'] == 'plain') {
    header("Content-type: text/plain");
    
    // Just provide raw list of active servers in the database.
    foreach($db->query("SELECT hostname FROM servers WHERE active=1".(isset($_GET['only_public']) ? " AND category <> \"Private\"" : "")) as $row) {
        echo $row['hostname'] . "\n";
    }
    
    // Done.

    
  } else if(isset($_GET['recalculate_key']) && $_GET['recalculate_key'] == $recalculate_secret_key && isset($_GET['recalculate_period'])) {
  
  
    $prefix = '';
    $maxDiff = -60*24*14;  /* 60*24*14 minutes = 14 days */
      
    if($_GET['recalculate_period'] == 'short') {
      $prefix = '_short';
      $maxDiff = -60;   /* 60 minutes = 1 hour */
    }
          
    // Update the database.
    $db->beginTransaction();
    
    $db->query('UPDATE server_statistics AS t1, (SELECT hostname, measured_from, SUM(CASE WHEN response_code=200 THEN 1 ELSE 0 END) AS successful, COUNT(*) as measurements, SUM(response_time) AS sum_response_time FROM server_timings WHERE measured_when >= TIMESTAMPADD(MINUTE, '.$maxDiff.', NOW()) GROUP BY hostname, measured_from) AS t2 SET t1.measurements'.$prefix.'=t2.measurements, t1.successful'.$prefix.'=t2.successful, t1.sum_response_time'.$prefix.'=t2.sum_response_time WHERE t1.hostname = t2.hostname AND t2.measured_from=t1.measured_from');
            
    // Commit changes.
    $db->commit();   
    
    // Generate warnings for reporters from which no data has been received in the last 24 hours (only for "long" recalculation)
    if($_GET['recalculate_period'] != 'short') {
      $stmt = $db->prepare('SELECT measured_from, MAX(measured_when) AS max_measured_when FROM server_timings GROUP BY measured_from HAVING max_measured_when < TIMESTAMPADD(HOUR, -24, NOW())');
      $stmt->execute(array());
      $missing_reports = $stmt->fetchAll();
      
      if(count($missing_reports) > 0) {
        // At least one reporter stopped reporting
        $mail_text = "The following reporters did not report any data in the last 24 hours:\n\n";
        
        foreach($missing_reports as $missing) {
          $mail_text .= '- '.$missing['measured_from'].'\n';
        }
        
        mail($warnings_email, "Response time reporter(s) stopped resporting", $mail_text, "From: Matrix Server List <".$warnings_email.">");
      }
    }
  
    // Done.
    echo "Done.";
  
  } else {
    // Gather timing statistics 
    $timing_pre_database = microtime(true)*1000;
  
    // Prepare fancy output.
    $stmt = $db->prepare('SELECT t1.*, t2.last_response, t2.last_response_time, t2.status_since, t2.last_versions, t2.measurements, t2.successful, t2.sum_response_time, t2.measurements_short, t2.successful_short, t2.sum_response_time_short, t3.users_active, t4.server_name, t4.server_version, t5.grade, t5.gradeTrustIgnored, t5.hasWarnings, t6.public_room_count FROM servers t1 LEFT JOIN server_statistics t2 ON t2.hostname=t1.hostname AND t2.measured_from=? LEFT JOIN server_users t3 ON t3.hostname=t1.hostname AND t3.current=1 LEFT JOIN server_version t4 ON t4.hostname=t1.hostname AND t4.current=1 LEFT JOIN server_security t5 ON t5.hostname=t1.hostname AND t5.current=1 LEFT JOIN server_room_count t6 ON t6.hostname=t1.hostname AND t6.current=1 WHERE t1.active=1 AND t1.category <> "Private" ORDER BY t1.hostname');
    $stmt->execute(array($show_from));
    $public_servers = $stmt->fetchAll();
    
    $stmt = $db->prepare('SELECT t1.*, t2.last_response, t2.last_response_time, t2.status_since, t2.last_versions, t2.measurements, t2.successful, t2.sum_response_time, t2.measurements_short, t2.successful_short, t2.sum_response_time_short, t3.users_active, t4.server_name, t4.server_version, t5.grade, t5.gradeTrustIgnored, t5.hasWarnings, t6.public_room_count FROM servers t1 LEFT JOIN server_statistics t2 ON t2.hostname=t1.hostname AND t2.measured_from=? LEFT JOIN server_users t3 ON t3.hostname=t1.hostname AND t3.current=1 LEFT JOIN server_version t4 ON t4.hostname=t1.hostname AND t4.current=1 LEFT JOIN server_security t5 ON t5.hostname=t1.hostname AND t5.current=1 LEFT JOIN server_room_count t6 ON t6.hostname=t1.hostname AND t6.current=1 WHERE t1.active=1 AND t1.category="Private" ORDER BY t1.hostname');
    $stmt->execute(array($show_from));
    $private_servers = $stmt->fetchAll();
    
    // Gather timing statistics 
    $timing_pre_last_timing = microtime(true)*1000;
    
    $stmt = $db->prepare('SELECT MAX(measured_when) AS last_timing FROM server_timings WHERE measured_from=?');
    $stmt->execute(array($show_from));
    $last_timing_row = $stmt->fetch();
    
    if($last_timing_row) {
      $last_timing = $last_timing_row['last_timing'];
    } else {
      $last_timing = '';
    }
    
    // Gather timing statistics 
    $timing_pre_template = microtime(true)*1000;
    
    require('public_servers.tmpl.php');
  
  }
  
} catch(PDOException $ex) {
   // Roll back if we are within a transaction
   try { $db->rollBack(); } catch (Exception $e2) {}

    // Return error.
    http_response_code(500);
    echo "An Error occured!\n"; //user friendly message
    echo $ex->getMessage()."\n";
}


?>