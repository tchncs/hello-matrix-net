<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Unofficial list of public Matrix servers</title>
	
		<style>
		 body {
		   font-family: Helvetica, Arial, Helvetica, sans-serif;
		   margin: 5%;
		 } 
		 
		 table {
		   width: 100%;
		 }
		 
		 td,th {
		   padding-left: 5px;
		   padding-right: 5px;
		 }
		 
		 .server-list {
		   font-size: small;
		 }
		 
		 .table-code {
		   font-family: Courier New, Courier, fixed-width;
		 }
		 
		 .table-num {
		   text-align: right;
		 }
		 
		 .table-indicator {
		   text-align: center;
		 }
		 
		 .table-footer-right {
		   text-align: right;
		   font-size: small;
		   font-style: italic;
		   padding: 10px;
		 }
		 
		 .ssl-grade-green a {
		   font-weight: bold;
		   color: #4EC83D;
		 }
		 
		 .ssl-grade-orange a {
		   font-weight:bold;
		   color: #FFA100;
		 }
		 
		 .ssl-grade-red a {
		   font-weight:bold;
		   color: #EF251E;
		 }
		 
		 li {
		   margin-bottom: 10px; 
		 }
		 
		 .location-selector {
		   font-size: small;
		   float: right;
		   margin-left: 15px;
		   margin-bottom: 15px;
		 }
		 
		 .server-online {
		   color: #4EC83D;
		 }
		 
		 .server-offline {
		   color: #EF251E;
		 }
		 
		 @media screen and (max-width: 900px) {
		   .hide-on-small {  
		    display: none;
		   }
		   
		   body {
		    margin-left: 1%;
		    margin-right: 1%;
		   }
		 }
		 
		 @media screen and (max-width: 650px) {
		   .hide-on-tiny {
		     display: none;
		   }
		 }
		
		</style>
		
		<link href='tablesort.css' rel='stylesheet'>
	</head>
<body>
<h1>Unofficial list of public Matrix servers</h1>

<div class="location-selector">
  <form action="public_servers.php" method="GET">
    <select name="show_from" onchange="this.form.submit()">
     <?php foreach($secret_keys as $secret_key => $location) { ?>
       <option<?=($location == $show_from ? ' selected' : '')?>><?=$location?></option>
     <?php } ?>
    </select>
    
    <noscript>
     <input type="submit" value="Go">
    </noscript>
  </form>
</div>
 
<p>This list provides an unofficial overview over public Matrix servers. We only list servers whose owners have explicitly asked us to do so. If you want to appear on the list, you can fill out <a href="http://framaforms.org/add-server-to-matrix-server-list-1484757063">our submission form</a>. In any case, you can also always join us in <a href="https://matrix.to/#/#hello-matrix:matrix.org">#hello-matrix:matrix.org</a> to get in touch.</p>

<table class="server-list" id="public-servers">
<thead>
<tr>
 <th>Hostname</th>
 <th>Description</th>
 <th>Country</th>
 <th class="hide-on-small">Since</th>
 <th class="hide-on-tiny">Type</th>
 <th class="hide-on-small">Software</th>
 <th class="hide-on-small">API Versions</th>
 <th>Last Status</th>
 <th class="hide-on-tiny">Reliability</th>
 <th class="hide-on-small">Avg Response</th>
 <th class="hide-on-tiny">Users</th>
 <th class="hide-on-small">Public Aliases</th>
 <th class="hide-on-tiny">SSL Labs</th>
</tr>
</thead>
<tbody>
<?php foreach($public_servers as $server) { ?>
<tr>
 <td class="table-code"><?=$server['hostname']?></td>
 <td class="table-text">
  <?=($server['url'] ? '<a href="'.$server['url'].'">' : '')?>
  <?=$server['description']?>
  <?=($server['url'] ? '</a>' : '')?>
 </td>
 <td class="table-indicator"><?=$server['location']?></td>
 <td class="table-indicator hide-on-small"><?=$server['online_since']?></td>
 <td class="table-text hide-on-tiny"><?=$server['category']?></td>
 <td class="table-text hide-on-small"><?=($server['server_name'] ? $server['server_name']." ".$server['server_version'] : $server['server'])?></td>
 <td class="table-text hide-on-small"><?=$server['last_versions']?></td>
 <td class="table-indicator <?=($server['last_response'] == 200 ? "server-online" : "server-offline")?>" title="<?=($server['last_response'] == 200 ? "Online" : "Offline")." since ".gmdate('Y-m-d H:i', strtotime($server['status_since']))?>"><?=($server['last_response'] == 200 ? "Online" : "Offline")?></td>
 <td class="table-num hide-on-tiny" title="Shows 14 days. Last hour: <?=($server['measurements_short'] > 0 ? number_format($server['successful_short'] * 100 / $server['measurements_short'], 1) : "n/a") ?> %"><?=($server['measurements'] > 0 ? number_format($server['successful'] * 100 / $server['measurements'], 1) : "n/a") ?> %</td>
 <td class="table-num hide-on-small" title="Shows 14 days. Last hour: <?=($server['successful_short'] > 0 ? number_format($server['sum_response_time_short'] / $server['successful_short'], 1) : "n/a")?> ms"><?=($server['successful'] > 0 ? number_format($server['sum_response_time'] / $server['successful'], 1) : "n/a")?> ms</td>
 <td class="table-num hide-on-tiny"><?=($server['users_active'] ? number_format($server['users_active'], 0) : 'n/a')?></td>
 <td class="table-num hide-on-small"><?=($server['public_room_count'] ? number_format($server['public_room_count'], 0) : 'n/a')?></td>
 <td class="table-indicator hide-on-tiny ssl-grade-<?=((!$server['hasWarnings'] && substr($server['grade'], 0, 1) == 'A') ? 'green' : ($server['grade'] == 'F' || $server['grade'] == 'T' ? 'red' : 'orange'))?>" data-sort="<?=(isset($sslGradeMap[$server['grade']]) ? $sslGradeMap[$server['grade']] : 1000)?>"><a href="https://www.ssllabs.com/ssltest/analyze.html?d=<?=$server['hostname']?>" target="_blank"><?=($server['grade'] ? $server['grade'].($server['hasWarnings'] ? " (!)" : "") : "n/a")?></a></td>
</tr> 
<?php } ?>
</tbody>
</table>

<div class="table-footer-right">
  Last updated: <?=gmdate('Y-m-d H:i', strtotime($last_timing))?> UTC
</div>

<p>The table provides the following information:</p>

<ul>
 <li><b>Type</b> indicates whether this server offers paid vs. free registration and also whether it is a private individual's experiment or run by an established organization that provides for its upkeep. In general, all free servers will be provided &quot;as is&quot; without warranties of any kind.</li>
 
 <li><b>Software</b> shows the software the server is running, as returned by the server's <code>/_matrix/federation/v1/version</code> endpoint.</li>
 
 <li><b>API Versions</b> is the data returned by the server's <code>/_matrix/client/versions</code> endpoint and shows the supported Matrix API versions.</li>
 
 <li><b>Last Status</b> shows whether the last poll on the <code>/_matrix/client/versions</code> endpoint was successful ("Online") or unsuccessful ("Offline"). We poll every five minutes. If you hover over the item it shows when the status last changed. Times are given in UTC.</li>
 
 <li><b>Reliability</b> is based on the same polling. It shows the percentage of requests that were successful (returned with status code 200) over the last 14 days the server was listed.</li>
 
 <li><b>Avg Response</b> shows the average time it took to poll the above. You can select the timing source you want to use with the drop-down on the top right. You have currently selected timings from <?=$show_from?>. In general, you should choose a timing source that is as close as possible to you (ideally at least on the same continent). Mouseover will display the average for the last hour.</li>
 
 <li><b>Users</b> are self-reported numbers and only available for servers whose operators  are providing us with these numbers.</li>
 
 <li><b>Public Aliases</b> shows the number of published aliases in that homeserver's public room directory and is based on the `total_room_count_estimate` returned by the servers' APIs. We update this number once a day.</li>
 
 <li><b>SSL Labs</b> shows the most recent rating of the SSL/TLS configuration of the server's client interface according to <a href="https://www.ssllabs.com/ssltest/">Qualys' SSL Labs</a> (updated about once a week).</li>
</ul>

<hr>

<p>The following servers are private, i.e. closed for user registrations, but its owners have nonetheless granted us permission to be listed here. Closed servers also form an important part of the Matrix ecosystem (as they host rooms and users) and their health can thus also be of public interest:</p>

<table class="server-list" id="private-servers">
<thead>
<tr>
 <th>Hostname</th>
 <th>Description</th>
 <th>Country</th>
 <th class="hide-on-small">Since</th>
 <th class="hide-on-tiny">Type</th>
 <th class="hide-on-small">Software</th>
 <th class="hide-on-small">API Versions</th>
 <th>Last Status</th>
 <th class="hide-on-tiny">Reliability</th>
 <th class="hide-on-small">Avg Response</th>
 <th class="hide-on-tiny">Users</th>
 <th class="hide-on-small">Public Aliases</th>
 <th class="hide-on-tiny">SSL Labs</th>
</tr>
</thead>
<tbody>
<?php foreach($private_servers as $server) { ?>
<tr>
 <td class="table-code"><?=$server['hostname']?></td>
 <td class="table-text">
  <?=($server['url'] ? '<a href="'.$server['url'].'">' : '')?>
  <?=$server['description']?>
  <?=($server['url'] ? '</a>' : '')?>
 </td>
 <td class="table-indicator"><?=$server['location']?></td>
 <td class="table-indicator hide-on-small"><?=$server['online_since']?></td>
 <td class="table-text hide-on-tiny"><?=$server['category']?></td>
 <td class="table-text hide-on-small"><?=($server['server_name'] ? $server['server_name']." ".$server['server_version'] : $server['server'])?></td>
 <td class="table-text hide-on-small"><?=$server['last_versions']?></td>
 <td class="table-indicator <?=($server['last_response'] == 200 ? "server-online" : "server-offline")?>" title="<?=($server['last_response'] == 200 ? "Online" : "Offline")." since ".gmdate('Y-m-d H:i', strtotime($server['status_since']))?>"><?=($server['last_response'] == 200 ? "Online" : "Offline")?></td>
 <td class="table-num hide-on-tiny" title="Shows 14 days. Last hour: <?=($server['measurements_short'] > 0 ? number_format($server['successful_short'] * 100 / $server['measurements_short'], 1) : "n/a") ?> %"><?=($server['measurements'] > 0 ? number_format($server['successful'] * 100 / $server['measurements'], 1) : "n/a") ?> %</td>
 <td class="table-num hide-on-small" title="Shows 14 days. Last hour: <?=($server['successful_short'] > 0 ? number_format($server['sum_response_time_short'] / $server['successful_short'], 1) : "n/a")?> ms"><?=($server['successful'] > 0 ? number_format($server['sum_response_time'] / $server['successful'], 1) : "n/a")?> ms</td>
 <td class="table-num hide-on-tiny"><?=($server['users_active'] ? number_format($server['users_active'], 0) : 'n/a')?></td>
 <td class="table-num hide-on-small"><?=($server['public_room_count'] ? number_format($server['public_room_count'], 0) : 'n/a')?></td>
 <td class="table-indicator hide-on-tiny ssl-grade-<?=((!$server['hasWarnings'] && substr($server['grade'], 0, 1) == 'A') ? 'green' : ($server['grade'] == 'F' || $server['grade'] == 'T' ? 'red' : 'orange'))?>" data-sort="<?=(isset($sslGradeMap[$server['grade']]) ? $sslGradeMap[$server['grade']] : 1000)?>"><a href="https://www.ssllabs.com/ssltest/analyze.html?d=<?=$server['hostname']?>" target="_blank"><?=($server['grade'] ? $server['grade'].($server['hasWarnings'] ? " (!)" : "") : "n/a")?></a></td>
</tr> 
<?php } ?>
</tbody>
</table>

<p>If you want to get added to this list let us know. We can then also set you up with notification emails which will be sent when your server cannot be reached from our test host anymore.</p>

<!-- Table sorting JavaScript - from https://github.com/tristen/tablesort -->
<script src='tablesort.min.js'></script>

<!-- Include sort types you need -->
<script src='tablesort.number.js'></script>

<script>
  new Tablesort(document.getElementById('public-servers'));
  new Tablesort(document.getElementById('private-servers'));
</script>

</body>
</html>

<!--
  Before database: <?=($timing_pre_database-$timing_start)?>ms
  Before last timing: <?=($timing_pre_last_timing-$timing_pre_database)?>ms
  Before template: <?=($timing_pre_template-$timing_pre_last_timing)?>ms
  Until now: <?=(microtime(true)*1000-$timing_pre_template)?>ms
-->
